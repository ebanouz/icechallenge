package helper;

import com.elements.Packet;
import com.execution.PacketTask;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public class PacketTaskWithOrder extends PacketTask {

    //Tells when this task was executed
    public static BlockingQueue<Packet> orderQueue = new ArrayBlockingQueue<Packet>(10);
    private final Packet packet;

    public PacketTaskWithOrder(Packet packet) {
        super(packet);
        this.packet = packet;
    }


    @Override
    public void run() {
//        System.out.println("Adding to ordered test queue: " + packet.getType());
      orderQueue.add(packet);
    }

    public static BlockingQueue<Packet> getOrderQueue() {
        return orderQueue;
    }
}
