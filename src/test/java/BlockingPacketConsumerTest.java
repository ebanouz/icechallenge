import com.elements.Packet;
import com.enums.SourceType;
import com.execution.PacketTask;
import com.execution.consumer.BlockingPacketConsumer;
import helper.PacketTaskWithOrder;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by ebanouz on 10/4/18.
 *
 */

public class BlockingPacketConsumerTest {

    private PriorityBlockingQueue<Runnable> queue = new PriorityBlockingQueue<>();

    @Test
    public void testOrder_1() throws InterruptedException {

        BlockingPacketConsumer blockingPacketConsumer = new BlockingPacketConsumer(queue);

        PacketTaskWithOrder higherPrioTask = new PacketTaskWithOrder(new Packet(SourceType.MANAGEMENT, false));
        PacketTaskWithOrder lowerPrioTask = new PacketTaskWithOrder(new Packet(SourceType.USER, false));

        List<PacketTaskWithOrder> packets = Arrays.asList(higherPrioTask, higherPrioTask, lowerPrioTask);

        //We want to give the "producer" thread higher priority so in case of a new packet coming into the system we want the "producer" to handle the order of the queue first
        //There's still can be a case in which the producer received new packet with high priority and just before the insert to the queue a context switch occurs and one of the threads takes
        //the current head of the queue even though it has a lower priority
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);

        //Add to queue - will execute
        queue.addAll(packets);

        List<Packet> ordered = new ArrayList<>();
        TimeUnit.SECONDS.sleep(1);

        PacketTaskWithOrder.getOrderQueue().drainTo(ordered);

        //Check order
        System.out.println(ordered.size());
        Assert.assertTrue(ordered.size() == 3);

        Assert.assertTrue(ordered.get(0).getType().equals(SourceType.MANAGEMENT));
        Assert.assertTrue(ordered.get(1).getType().equals(SourceType.MANAGEMENT));
        Assert.assertTrue(ordered.get(2).getType().equals(SourceType.USER));

    }

    @Test
    public void testOrder_2() throws InterruptedException {

        BlockingPacketConsumer blockingPacketConsumer = new BlockingPacketConsumer(queue);

        PacketTaskWithOrder higherPrioTask = new PacketTaskWithOrder(new Packet(SourceType.MANAGEMENT, false));
        PacketTaskWithOrder lowerPrioTask = new PacketTaskWithOrder(new Packet(SourceType.USER, false));
        PacketTaskWithOrder higherPrioTaskLarge = new PacketTaskWithOrder(new Packet(SourceType.USER, true));
        PacketTaskWithOrder higherPrioTaskMLarge = new PacketTaskWithOrder(new Packet(SourceType.MANAGEMENT, false));

        List<PacketTaskWithOrder> packets = Arrays.asList(higherPrioTask, higherPrioTaskLarge, lowerPrioTask, higherPrioTaskMLarge);


        //We want to give the "producer" thread higher priority so in case of a new packet coming into the system we want the "producer" to handle the order of the queue first
        //There's still can be a case in which the producer received new packet with high priority and just before the insert to the queue a context switch occurs and one of the threads takes
        //the current head of the queue even though it has a lower priority
        Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
        //Add to queue - will execute
        queue.addAll(packets);

        List<Packet> ordered = new ArrayList<>();
        TimeUnit.SECONDS.sleep(1);

        PacketTaskWithOrder.getOrderQueue().drainTo(ordered);

        //Check size
//        Assert.assertTrue(ordered.size() == 4);

//        //Check order
//        Assert.assertTrue(ordered.get(0).getType().equals(SourceType.MANAGEMENT));
//        Assert.assertTrue(ordered.get(1).getType().equals(SourceType.MANAGEMENT));
//        Assert.assertTrue(ordered.get(2).getType().equals(SourceType.USER));
//        Assert.assertTrue(ordered.get(3).getType().equals(SourceType.USER));

        TimeUnit.SECONDS.sleep(1);
    }
}
