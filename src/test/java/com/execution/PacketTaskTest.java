package com.execution;

import com.elements.Packet;
import com.enums.SourceType;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by ebanouz on 10/4/18.
 */
public class PacketTaskTest {

    @Test
    public void compareTo() throws Exception {

        PacketTask higher = new PacketTask(new Packet(SourceType.MANAGEMENT, false));
        PacketTask lower = new PacketTask(new Packet(SourceType.USER, false));

        Assert.assertTrue(higher.compareTo(higher) == 0);
        Assert.assertTrue(higher.compareTo(lower) == -1);

        Assert.assertTrue(lower.compareTo(higher) == 1);

        //Same sourceType
        higher = new PacketTask(new Packet(SourceType.USER, false));
        lower = new PacketTask(new Packet(SourceType.USER, false));

        Assert.assertTrue(lower.compareTo(higher) == 0);

        //isLarge priority
        higher = new PacketTask(new Packet(SourceType.USER, true));
        lower = new PacketTask(new Packet(SourceType.USER, false));
        Assert.assertTrue(higher.compareTo(lower) == -1);
    }

}