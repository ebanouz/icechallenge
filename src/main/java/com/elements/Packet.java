package com.elements;

import com.enums.SourceType;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public class Packet {

    private final SourceType type;
    private final boolean isLarge;

    public Packet(SourceType type, boolean isLarge) {
        this.type = type;
        this.isLarge = isLarge;
    }

    public SourceType getType() {
        return type;
    }

    public boolean isLarge() {
        return isLarge;
    }


    @Override
    public String toString() {
        return "Packet{" +
                "type=" + type +
                ", isLarge=" + isLarge +
                '}';
    }
}
