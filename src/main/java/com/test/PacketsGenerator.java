package com.test;

import com.elements.Packet;
import com.enums.SourceType;

/**
 * Created by ebanouz on 10/4/18.
 * Used to generate random {@link Packet}
 */
public class PacketsGenerator {

    public static Packet generate() {
        return new Packet(Math.random() > 0.5 ? SourceType.MANAGEMENT : SourceType.USER,
                          Math.random() > 0.5);
    }

    public static void main(String[] args) {
        System.out.println(Math.random());
    }
}
