package com.io;

import com.elements.Packet;

import java.util.logging.Logger;


/**
 * Created by ebanouz on 10/4/18.
 * Allows packets to be entered into the system
 * Can be implemented as file reader, read from repository, etc
 */
public class PacketProducer implements Reader<Packet> {

    protected static final Logger logger = Logger.getLogger(PacketProducer.class.getName());

    public boolean read(Packet item) {

        if (item == null) {
            logger.warning("Skipping null packet");
            return false;
        }


        return false;
    }
}
