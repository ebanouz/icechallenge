package com.io;

/**
 * Created by ebanouz on 10/4/18.
 * Reads elements of type T. "read" can be either
 */
public interface Reader<T> {

    /**
     *
     * @param item
     * @return true upon a successful read and false otherwise
     */
    boolean read(T item);
}
