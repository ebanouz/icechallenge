package com.io.queue;

import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.logging.Logger;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public class PacketsQueue<T extends Runnable> {  //Generic - In case we want to extend packet and change priorities

    protected static final Logger logger = Logger.getLogger(PacketsQueue.class.getName());

    private static final int DEFAULT_CAPACITY = 100_000;

    PriorityBlockingQueue<T> queue = new PriorityBlockingQueue<T>(DEFAULT_CAPACITY);

    public boolean add(T packet) {
        try {
            return queue.add(packet);
        } catch (Exception e) {
            logger.warning("Error adding new item to the queue: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    public PriorityBlockingQueue<T> getQueue() {
        return queue;
    }

    public boolean addAll(List<T> packets) {
        try {
            if (packets == null || packets.size() == 0) {
                logger.warning("Noting to add - empty list");
                return false;
            }

            return queue.addAll(packets);
        } catch (Exception e) {
            logger.warning("Error adding items to the queue: " + e.getMessage());
            e.printStackTrace();
            return false;
        }
    }

    // Please provide both blocking (if the system is empty it would wait some timeout for a packet to be routed rather than returning immediately with no packet) and non-blocking (return null with no packet) implementations of this method
}
