package com.enums;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public enum SourceType {

    MANAGEMENT(1),
    USER(0);

    //Higher value = higher priority
    private final int priority;

    SourceType(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }
}
