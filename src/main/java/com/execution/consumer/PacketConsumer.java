package com.execution.consumer;

import com.elements.Packet;
import com.io.queue.PacketsQueue;

import java.util.concurrent.*;
import java.util.logging.Logger;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public abstract class PacketConsumer<T extends Packet> {

    boolean running = true;

    protected static final Logger logger = Logger.getLogger(PacketConsumer.class.getName());


    final ThreadPoolExecutor executor;
    final PriorityBlockingQueue<Runnable> queue;


    public PacketConsumer(PriorityBlockingQueue<Runnable> packetTaskPacketsQueue) {
        this.executor = new ThreadPoolExecutor(5, 10, 1000, TimeUnit.MILLISECONDS, packetTaskPacketsQueue);
        this.queue = packetTaskPacketsQueue;

        init();
    }

    abstract void init();

    public void shutdown() {
        try {
            running = false;
            executor.shutdown();

                if (!executor.awaitTermination(1, TimeUnit.SECONDS)) {
                    executor.shutdownNow();
                }

        } catch (Exception e) {
            logger.warning("Failed to close executer service gracefully");
            e.printStackTrace();
        }
    }

}


