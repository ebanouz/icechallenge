package com.execution.consumer;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.RejectedExecutionException;

/**
 * Created by ebanouz on 10/4/18.
 */
public class BlockingPacketConsumer extends PacketConsumer {

    public BlockingPacketConsumer(PriorityBlockingQueue<Runnable> packetTaskPacketsQueue) {
        super(packetTaskPacketsQueue);
    }

    //There few ways to implement this part - can use a different thread for the "producer", make all the pool's thread wait on the queue, etc.
    @Override
    void init() {
        new Thread() {
            @Override
            public void run() {
                while (running) {
                    try {
                        Runnable task = (Runnable) queue.take();
                        executor.execute(task);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (RejectedExecutionException e) {
                        logger.warning("Can't process new packets");
                    }
                }
            }
        }.start();
    }
}
