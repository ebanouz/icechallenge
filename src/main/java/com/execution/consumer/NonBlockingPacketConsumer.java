package com.execution.consumer;

import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * Created by ebanouz on 10/4/18.
 * non-blocking (return null with no packet) implementations of this method
 */

public class NonBlockingPacketConsumer extends PacketConsumer {

    public NonBlockingPacketConsumer(PriorityBlockingQueue<Runnable> packetTaskPacketsQueue) {
        super(packetTaskPacketsQueue);
    }

    //There few ways to implement this part - can use a different thread for the "producer", make all the pool's thread wait on the queue, etc.
    @Override
    void init() {
        new Thread() {
            @Override
            public void run() {
                while (running) {
                    try {

                        Runnable task = (Runnable) queue.poll(1, TimeUnit.SECONDS);
                        if (task != null) {
                            executor.execute(task);
                        }
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }.start();
    }
}
