package com.execution;

import com.elements.Packet;
import com.enums.SourceType;

import java.util.logging.Logger;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public class PacketTask<T extends Packet> implements Runnable, Comparable<PacketTask> {

    protected static final Logger logger = Logger.getLogger(PacketTask.class.getName());

    private final T packet;

    public PacketTask(T packet) {
        this.packet = packet;
    }

    public T getPacket() {
        return packet;
    }

    @Override
    public void run() {
//        logger.info("Removing packet: <" + packet.toString() + "> from the system" );
    }

    @Override
    public int compareTo(PacketTask other) {


        //Management has higher priority than User
        if (packet.getType().equals(SourceType.USER) && other.getPacket().getType().equals(SourceType.MANAGEMENT)) {
            return 1;
        } else if (packet.getType().equals(SourceType.MANAGEMENT) && other.getPacket().getType().equals(SourceType.USER)) {
            return -1;

          //For packets of same sourceType, isLarge being true has higher priority than false
        } else if (packet.getType().equals(other.getPacket().getType())) {
            if (packet.isLarge() && !other.getPacket().isLarge()) {
                return -1;
            } else if (!packet.isLarge() && other.getPacket().isLarge()) {
                return 1;
            }
        }

        //All else being equal, packets that have been in the queue longer should be removed before packets that have not been waiting as long
        return 0;
    }
}
