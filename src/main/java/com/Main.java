package com;

import com.elements.Packet;
import com.execution.PacketTask;
import com.execution.consumer.BlockingPacketConsumer;
import com.execution.consumer.PacketConsumer;
import com.io.PacketProducer;
import com.test.PacketsGenerator;

import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by ebanouz on 10/4/18.
 *
 */
public class Main {

    private static final int DEFAULT_CAPACITY = 100_000;

    public static void main(String[] args) throws InterruptedException {
        //Create a "Provider"
        PacketProducer packetProducer = new PacketProducer();

        //Create Queue
        PriorityBlockingQueue<Runnable> queue = new PriorityBlockingQueue<Runnable>(DEFAULT_CAPACITY);

        //Create executor
        PacketConsumer packetConsumer = new BlockingPacketConsumer(queue);

        //Create packets with the generator
        for (int i = 0 ; i < 10_000; i++) {
            queue.add(new PacketTask<Packet>(PacketsGenerator.generate()));
        }

        System.out.println("shutdown");
        packetConsumer.shutdown();


        Thread.currentThread().join();

    }
}
